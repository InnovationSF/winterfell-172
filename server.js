const express = require('express')
const app = express()
const port = 3000
var http = require("http").Server(app);

//Socket.io
var io = require("socket.io")(http); 

/* Google Drive API */
const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const OAuth2Client = google.auth.OAuth2;
const SCOPES = ['https://www.googleapis.com/auth/drive'];
const TOKEN_PATH = 'credentials.json';/*-------------------*/


//Library for database interaction
var pgp = require('pg-promise')(/*options*/)
var db = pgp('postgres://postgres:Si1hou3tt3@localhost:5432/winterfell')
app.use(express.static(__dirname + "/public" ));

// Sets homepage to index.html
app.get('/', function(req, res) {
  res.redirect('index.html');
});
 
io.on('connection', function(socket){
	/* --------------ALL SOCKET IO --------------*/
	//On Page Load

	socket.on('init',function(nothing){
		 db.many('SELECT * from inventory')
		  .then(function (data) {
		    socket.emit('start', data); 
		  	data.forEach(function(obj) {  		
			})
		  })
	});
	// On add action
	socket.on('add',function(data){
		db.none('INSERT INTO inventory(name, cost,stock, description) VALUES(${name}, ${cost}, ${stock}, ${description})', {
	    	name: data.name,
	    	cost: data.cost,
	    	stock: data.stock,
	    	description: data.description  
		})
	});
	// On del action
	socket.on('delete',function(data){
		db.none('DELETE FROM inventory WHERE name=${name}', {
	    	name: data,
		})
	});	

	socket.on('upload',function(data){
		// Load client secrets from a local file.
		fs.readFile('client_secret.json', (err, content) => {
		  if (err) return console.log('Error loading client secret file:', err);
		  // Authorize a client with credentials, then call the Google Drive API.
		  authorize(socket, JSON.parse(content), listFiles);
		});

	});	
	socket.on('download',function(data){
		 var csv = "";
		 db.many('SELECT * from inventory')
		  .then(function (data) {
		    csv += "Name, Cost, Stock, Description\n";
		  	data.forEach(function(obj) { 
		  	 	csv += obj.name;
		  	 	csv += ",";
		  	 	csv += obj.cost;
		  	 	csv += ",";
		  	 	csv += obj.stock;
		  	 	csv += ",";
		  	 	csv += obj.description;
		  	 	csv += "\n";
			})
			fs.writeFile("inventory.csv", csv);
			socket.emit('downloadfinished', {message:"done"}); 
		  })  
	});		
}) 

// Open webserver on port 3000
http.listen(port,function(){
    console.log('Server listening through port %s', port);
}); 

// ALL FUNCTIONS BELOW ARE GOOGLE DRIVE AUTH

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(socket, credentials, callback) {
      var client_secret = credentials.web.client_secret;
  var client_id = credentials.web.client_id;
  var redirect_uris = credentials.web.redirect_uris;
  //const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new OAuth2Client(client_id, client_secret, "http://localhost:3000/inventory.html");

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getAccessToken(socket, oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client,socket);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getAccessToken(socket,oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  socket.emit('auth', authUrl); 
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return callback(err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

/**
 * Lists the names and IDs of up to 10 files.
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listFiles(auth,socket) {
  const drive = google.drive({version: 'v3', auth});
  var fileMetadata = {
	'name': 'inventory.csv'
  };
		 var csv = "";
		 db.many('SELECT * from inventory')
		  .then(function (data) {
		    csv += "Name, Cost, Stock, Description\n";
		  	data.forEach(function(obj) { 
		  	 	csv += obj.name;
		  	 	csv += ",";
		  	 	csv += obj.cost;
		  	 	csv += ",";
		  	 	csv += obj.stock;
		  	 	csv += ",";
		  	 	csv += obj.description;
		  	 	csv += "\n";
			})
  var media = {
			  mimeType: 'text/csv',
			  body: csv
   };
	drive.files.create({
			  resource: fileMetadata,
			  media: media,
			  fields: 'id'
			}, function (err, file) {
			  if (err) {
			    // Handle error
			    console.error(err);
			  } else {
			    console.log('File uploaded.');
			    socket.emit('uploadfinished', {message: "done"}); 
			  }
	});  

		  })


}