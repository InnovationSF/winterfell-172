# winteriscoming-project

## by Andrew H. Dmitriy M. Imaan T. Rana S. Raghav G.

		Winterfell is an enterprise application where a user can
		manage a wholesale inventory through our user-friendly
		interface. Winterfell allows users to maintain inventory items, change costs, details of customers, select stock to buy and alert when fallen below a certain threshold.  

		Additional Features include:
		Jenkins/Ansible Integration with Github
		Google Drive Integration